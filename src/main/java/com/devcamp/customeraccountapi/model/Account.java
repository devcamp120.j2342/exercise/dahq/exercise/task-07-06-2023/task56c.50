package com.devcamp.customeraccountapi.model;

public class Account {
    private int id;
    private double balance = 0.0;
    private Customer customer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Account(int id, double balance, Customer customer) {
        this.id = id;
        this.balance = balance;
        this.customer = customer;
    }

    public Account(int id, double balance) {
        this.id = id;
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Account [id=" + id + ", balance=" + balance + "$" + ", customer=" + customer + "]";
    }

    public Account deposit(double amount) {
        Account account = new Account(id, balance, customer);
        account.setBalance(amount + balance);
        return account;
    }

    public Account withdraw(double amount) {
        Account account = new Account(id, balance, customer);
        if (this.balance >= amount) {
            account.setBalance(this.balance - amount);
        } else {
            System.out.println("amount withdraw exceeds the curren balance");
        }
        return account;
    }

}
