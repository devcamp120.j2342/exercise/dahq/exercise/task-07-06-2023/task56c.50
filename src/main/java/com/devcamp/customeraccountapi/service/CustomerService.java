package com.devcamp.customeraccountapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customeraccountapi.model.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer(1, "Nguyen Van A", 10);
    Customer customer2 = new Customer(2, "Nguyen Van B", 20);
    Customer customer3 = new Customer(3, "Nguyen Van C", 30);

    public ArrayList<Customer> allCustomers() {
        ArrayList<Customer> customers = new ArrayList<>();
        customers.add(customer1);
        customers.add(customer2);
        customers.add(customer3);
        return customers;
    }
}
