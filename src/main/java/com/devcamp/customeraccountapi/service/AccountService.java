package com.devcamp.customeraccountapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.customeraccountapi.model.Account;

@Service
public class AccountService {
    @Autowired
    private CustomerService customerService;
    Account account1 = new Account(1, 1000);
    Account account2 = new Account(2, 4500);
    Account account3 = new Account(3, 2000);

    public ArrayList<Account> allAccounts() {
        account1.setCustomer(customerService.customer1);
        account2.setCustomer(customerService.customer2);
        account3.setCustomer(customerService.customer3);
        ArrayList<Account> accounts = new ArrayList<>();
        accounts.add(account1);
        accounts.add(account2);
        accounts.add(account3);
        return accounts;

    }

}
