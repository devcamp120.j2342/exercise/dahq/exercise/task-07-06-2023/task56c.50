package com.devcamp.customeraccountapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customeraccountapi.model.Customer;
import com.devcamp.customeraccountapi.service.CustomerService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping("/customers")
    public ArrayList<Customer> customersList() {
        ArrayList<Customer> customers = customerService.allCustomers();
        return customers;
    }

}
